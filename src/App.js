
import './App.css';
import {
  BrowserRouter as Router,
} from "react-router-dom";
import RouterUrl from './Router/RouterUrl';

function App(props) {
  return (
    <Router>
    <div className="App">
      <RouterUrl></RouterUrl>
    </div>
    </Router>
  );
}

export default App;
