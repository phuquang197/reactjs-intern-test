export default function validate(values) {
  let errors = {};
  if (!values.client_id) {
    errors.client_id = 'Client id không được để trống';
  } 
  if (!values.client_secret) {
    errors.client_secret = 'Client_secret không được để trống';
  } 
  
  return errors;
};