import React from 'react';
function Card(props) {
    const {animal} = props;
    return (
        <div>
            <div className="Card">
                <div className="Card-left">
                    <img className="Card-left__img" src={animal.photos.length ? animal.photos[0].full : 'https://firebasestorage.googleapis.com/v0/b/upimg-28822.appspot.com/o/17740ea3d0381c495391932628423915-removebg-preview.png?alt=media&token=86255af9-ab67-4f89-84d6-9026486e6f1f' }></img>
                </div>
                <div className="Card-right">
                    <div><h2 className="Card-right__title"> <a href="#a">{animal.name}</a></h2></div>
                    <div className="Card-right__infor">
                    <div className="Card-right__gender">Gender: {animal.gender}</div>
                    <div className="Card-right__age">Age: {animal.age}</div>
                    </div>
                    <b>Contact:</b><br></br>
                    <small>Email: {animal.contact.email} </small><br></br>
                    <small>Phone: {animal.contact.phone} </small><br></br>
                    <small>Address: {animal.contact.address.address1}, {animal.contact.address.city} </small><br></br>

                    <div className="Card-right__descridivtion">
                        {animal.description}
                    </div>
                    
                </div>
            </div> 
        </div>
    );
}

export default Card;