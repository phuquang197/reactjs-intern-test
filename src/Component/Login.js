// import React from 'react';
import React from "react";
import axios from "axios";
import validate from "../Validation/ValidaationLogin";
import useForm from "../Validation/userForm";
import qs from 'qs';
function Login() {
  const token = localStorage.getItem("access_token");
  const { values, errors, handleChange, handleSubmit, } = useForm
    (login,
    validate);
  const checkLogin = async () => {
    try {
      const data = {
        grant_type: 'client_credentials',
        client_id: values.client_id,
        client_secret: values.client_secret,
      }
      
      const result = await axios({
        method: 'post',
        url: 'https://api.petfinder.com/v2/oauth2/token',
        data: qs.stringify(data),
        headers: {
          "Content-Type": "application/x-www-form-urlencoded"
        }
      });
      localStorage.setItem("access_token", result.data.access_token);
      console.log(result);
    
    } catch (e) {
       alert('Client id hoặc Client Secret sai vui lòng nhập lại ');
    }
  }

  function login() {

    if (token) {
      window.location.href = '/Home';
    }
  }

  return (
    <div style={{
      width:'1519.200px',
      height:'680px',
      backgroundColor:"#1DA1F2",
      paddingTop:'50px'
    }}>
      <div className="login">
      <div className="img-login">
      </div>
      <form className="form-login" onSubmit={handleSubmit} noValidate>
        <h2 style={{textAlign:"center",fontSize:"46px !important"}} className="logo_login">Petfinder</h2>
        <h2>LOGIN</h2>
        <div className="field">
          <label className="title_login">Client id</label>
          <div className="control">
            <input autoComplete="off" className={`input ${errors.client_id && 'is-danger'}`} type="text" name="client_id" onChange={handleChange} value={values.client_id || ''} required />
            {errors.client_id && (
              <small className="help is-danger">{errors.client_id}</small>
            )}
            {/* <small className="help is-danger">{message}</small> */}
          </div>
        </div>
        <div style={{ marginTop: "20px" }} className="field">
          <label className="title_login">Client Secret</label>
          <div className="control">
            <input className={`input ${errors.client_secret && 'is-danger'}`} type="client_secret" name="client_secret" onChange={handleChange} value={values.client_secret || ''}  required />
          </div>
          {errors.client_secret && (
            <small className="help is-danger">{errors.client_secret}</small>
          )}
        </div>
        <button type="submit" className="button-login is-block is-info is-fullwidth" onClick={() => checkLogin()} >Login</button>
        
      </form>
      </div>
    </div>
  );
}

export default Login;

