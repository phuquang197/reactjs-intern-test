import React from 'react';
function Menu(props) {
    const logOut = async () => {
        localStorage.removeItem("access_token");
        window.location.href = '/';
    }
  
    return (
        <div style={{backgroundColor:"#333",marginBottom:"23px"}}>
            <div className="navbar">
                <a className="logo" href="/Animals">Petfinder</a>
                <div className="dropdown_breeds">
                    <button className="dropbtn_breeds">BREED</button>
                    <div className="dropdown-content__breeds">
                        <a style={{minWidth:'160px', textAlign:'left'}} href="/dog">Dog</a> <br></br>
                        <a style={{minWidth:'160px', textAlign:'left'}} href="/cat">Cat</a>
                        
                    </div>
                </div>
                <div className="dropdown">
                    <button className="dropbtn"><i className="fa fa-user-circle" aria-hidden="true"></i></button>
                    <div className="dropdown-content">
                        <button className="btn-logout" onClick={logOut}>Đăng Xuất</button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Menu;