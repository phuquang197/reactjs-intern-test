import React from 'react';
import {
    Route,
}
from "react-router-dom";
import Login from '../Component/Login';
import Animals from '../Page/Animals';
import Cat from '../Page/Cat';
import Dog from '../Page/Dog';
function RouterUrl(props) {
    return (
        <div>
            <Route exact path="/"><Login></Login></Route>
            <Route path="/Home"><Animals></Animals> </Route>
            <Route path="/cat"><Cat></Cat></Route>
            <Route path="/dog"><Dog></Dog></Route>

        </div>
    );
}

export default RouterUrl;