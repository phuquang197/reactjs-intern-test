import React, { useEffect, useState } from "react";
import axios from "axios";
import Card from "../Component/Card";
import { makeid } from "../help/create_key_index";
import Menu from "../Component/Menu";
function Animals() {


    const [animals, setAnimals] = useState();
    const [page = 1, setPage] = useState();
    const token = localStorage.getItem("access_token");
    const nextPage = () => {
        setPage(page + 1);

    };
    const previousPage = () => {
        setPage(page - 1);
    };
    const fetchData = async () => {
        const result = await axios.get(`https://api.petfinder.com/v2/animals?page=${page}`, {

            headers: {
                Authorization: `Bearer ${token}`,
            },
        })

        return result.data
    }
    
    useEffect(() => {
        fetchData().then(data => {
            setAnimals(data)
        })
    }, [page])


    if (!animals || animals.lengh === 0) {
        return (<img src="https://i.pinimg.com/originals/ca/89/fb/ca89fbce5e5c68f46d5330946c58ddc6.gif"></img>)
    }
    console.log(animals);
    return (
        <>
            <Menu></Menu>
            <div className="background-card">
                {/* {animals.id} */}

                {
                    animals.animals.map((animal) => {
                        return <Card key={makeid(10)} animal={animal}></Card>
                    })
                }

                <div className="text-center">
                    <button className="btn_previousPage" 
                    disabled={page<=1}
                    onClick={previousPage
                    }>PREV</button>
                    <button className="btn_nextPage" onClick={nextPage}>NEXT</button>
                </div>
            </div>
        </>
    );
}

export default Animals;